import request from '@/helper/request'
import helper from '@/helper/helper'
// 登录接口（认证中心）
export const authLogin = data =>
  request({
    url: '/auth/user/login',
    method: 'post',
    data
  })
// 请求授权码接口（认证中心）
export const getAuthCode = params =>
  request({
    url: '/auth/oauth/authorize',
    method: 'post',
    headers: {
      token: helper.getToken()
    },
    params
  })
// 授权码获取token接口（认证中心生成业务系统token）/ 刷新token接口（认证中心，返回406时调用）
export const getAuthToken = params =>
  request({
    url: '/auth/oauth/token',
    method: 'post',
    params
  })
// 登出接口
export const userLogout = () =>
  request({
    url: '/auth/user/logout',
    method: 'post'
  })
// 业务系统---获取用户详细信息
export const getInfo = (clientId) =>
  request({
    url: `/system/user/getInfo/${clientId}`,
    method: 'get'
  })
