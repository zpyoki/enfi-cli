import request from '@/helper/request'

export const getExampleList = () => request({
  url: `/example/list`,
  method: 'GET'
})

export const getExampleDetail = (id) => request({
  url: `/example/${id}`,
  method: 'GET'
})

export const updateExample = (id) => request({
  url: `/example/update`,
  method: 'GET',
  params: {
    id: id
  }
})
