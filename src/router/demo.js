const LAYOUT = () => import('@/components/layout')

export default [
  {
    path: '/demo',
    name: 'demo',
    redirect: '/demo/table',
    component: LAYOUT,
    meta: {
      title: '基础DEMO',
      icon: 'menu-fold'
    },
    children: [
      {
        path: 'table',
        name: 'demoTable',
        component: () => import('@/views/demo/Basic.vue'),
        meta: {
          title: '表格'
        },
        children: [
          {
            path: 'table-detail',
            name: 'tableDetail',
            component: () => import('@/views/demo/detail.vue'),
            meta: {
              title: '表格详情',
              hidden: true,
              propActive: 'demoTable'
            }
          }
        ]
      }
    ]
  }
]
