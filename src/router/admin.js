const LAYOUT = () => import('@/components/layout')
export default [
  {
    path: '/',
    name: 'default',
    redirect: '/dashboard',
    component: LAYOUT,
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('@/views/dashboard'),
        meta: {
          title: '工作台',
          icon: 'workbench',
          affix: true
        }
      }
    ]
  }
  // {
  //   path: '/test3row',
  //   name: 'test3row',
  //   redirect: '/test3row',
  //   component: LAYOUT,
  //   meta: { hidden: true },
  //   children: [
  //     {
  //       path: 'test3row1',
  //       name: 'test3row1',
  //       component: () => import('@/views/test3row')
  //     },
  //     {
  //       path: 'test3row2',
  //       name: 'test3row2',
  //       component: () => import('@/views/test3row'),
  //       children: [
  //         {
  //           path: 'test3row3',
  //           name: 'test3row3',
  //           component: () => import('@/views/test3row')
  //         },
  //         {
  //           path: 'test3row4',
  //           name: 'test3row4',
  //           component: () => import('@/views/test3row')
  //         }
  //       ]
  //     }

  //   ]
  // },
  // {
  //   path: '/demoMenu',
  //   name: 'demoMenu',
  //   redirect: '/demoMenu/table',
  //   component: LAYOUT,
  //   meta: {
  //     title: '组件demo',
  //     icon: 'assembly'
  //   },
  //   children: [
  //     {
  //       path: 'basicpage',
  //       name: 'basicpage',
  //       component: () => import('@/views/Basicpage'),
  //       meta: {
  //         title: '基础详情页'
  //       }
  //     },
  //     {
  //       path: 'advancedpage',
  //       name: 'advancedpage',
  //       component: () => import('@/views/advancedpage'),
  //       meta: {
  //         title: '高级详情页'
  //       }
  //     },
  //     {
  //       path: 'search',
  //       name: 'search',
  //       component: () => import('@/views/searchlist/Projects.vue'),
  //       meta: {
  //         title: '查询表格'
  //       }
  //     },
  //     {
  //       path: 'inquire',
  //       name: 'inquire',
  //       component: () => import('@/views/Inquire'),
  //       meta: {
  //         title: '高级详情页'
  //       }
  //     },
  //     {
  //       path: 'customadd',
  //       name: 'customadd',
  //       component: () => import('@/views/customadd'),
  //       meta: {
  //         title: '自定义组件集合'
  //       }
  //     },
  //     {
  //       path: 'from',
  //       name: 'from',
  //       component: () => import('@/views/module/FormTest.vue'),
  //       meta: {
  //         title: 'from表单'
  //       }
  //     },
  //     {
  //       path: 'table',
  //       name: 'table',
  //       component: () => import('@/views/module/TableTest.vue'),
  //       meta: {
  //         title: 'table表格'
  //       }
  //     },
  //     {
  //       path: 'ajax',
  //       name: 'ajax',
  //       component: () => import('@/views/ajax'),
  //       meta: {
  //         title: 'ajax跳转'
  //       }
  //     }
  //   ]
  // }
]
