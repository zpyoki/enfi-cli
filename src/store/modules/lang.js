const lang = {
  namespaced: false,
  state: {
    language: 'zh',
    sider: 'dashboard'
  },
  getters: {

  },
  mutations: {
    SET_LANG (state, value) {
      state.language = value
    },
    SET_SIDER (state, value) {
      state.sider = value
    }
  },
  actions: {

  }
}

export default lang
