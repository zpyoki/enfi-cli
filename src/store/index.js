import Vue from 'vue'
import Vuex from 'vuex'
import User from './modules/user'
import Permission from './modules/permission'
import Lang from './modules/lang'
import tagsView from './modules/tagsView'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  actions: {},
  mutations: {
  },
  getters: {},
  modules: {
    User,
    Permission,
    Lang,
    tagsView
  }
})
