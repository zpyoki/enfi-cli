import helper from '@/helper/helper'
import store from '@/store'
import { intersection, isArray } from 'lodash'

const checkAuth = (t, s) => {
  return checkAround(t, s) && checkLast(t)
}

const checkAround = (t, s) => {
  return helper.tod(parseFloat(t) - parseFloat(helper.charge(s))) > process.env.VUE_APP_T
}

const checkLast = (t) => {
  return t.toString().substr(9, 1) > process.env.VUE_APP_S
}

export const usePermission = () => {
  function hasPermission (value, def = true) {
    if (!value || store.getters.roles.includes('admin')) {
      return def
    }
    if (!isArray(value)) {
      return store.getters.permissions.includes(value)
    }
    return (intersection(value, store.getters.permissions)).length > 0
  }
  return { hasPermission }
}

export default {
  checkAuth,
  checkAround,
  checkLast
}
