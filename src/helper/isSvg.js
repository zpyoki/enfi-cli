// 判断icon是不是自定义的svg
export const useSvg = () => {
  function isSvg (name) {
    const files = require.context('@/assets/icons/svg', true, /\.svg$/).keys()
    const allSvgName = files.map(item => item.match(/.\/(\S*).svg/)[1])
    if (allSvgName.includes(name)) {
      return true
    }
    return false
  }
  return { isSvg }
}
