
const en = {
  name: 'test',
  title: 'Good afternoon, Amano. I guess you may be tired',
  table: 'Table demo',
  work: 'workbench',
  form: 'form form',
  workbench: 'workbench',
  language: 'language',
  admin: 'admin',
  password: 'password',
  login: 'login',
  loginsuccess: 'loginsuccess',
  loginfail: 'loginfail',
  reset: 'reset',
  permission: 'Permission',
  pagePer: 'PagePermission',
  directivePer: 'DirectivePermission',
  userManage: 'UserManage',
  quit: 'Sign Out',
  errorPage: 'ErrorPage',
  system: 'system'
}
export default en
