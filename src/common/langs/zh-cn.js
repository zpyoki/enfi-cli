const zh = {
  text: '测试',
  title: '下午好，天野远子，我猜你可能累了',
  table: '表格 demo',
  work: '工作台',
  form: '表单form',
  workbench: '工作台',
  language: '语言',
  admin: '管理员',
  password: '密码',
  login: '登录',
  loginsuccess: '登录成功',
  loginfail: '登录失败',
  reset: '重置',
  permission: '权限设置',
  pagePer: '页面权限',
  directivePer: '按钮权限',
  userManage: '用户管理',
  quit: '退出',
  errorPage: '错误页面',
  system: '系统'

}
export default zh
