# ENFI-CLI

[![Home](https://www.enfi.com.cn/enfi/resource/cms/2017/11/img_pc_site/2017110510383950563.png)](https://www.enfi.com.cn)

完整的 [node.js](https://nodejs.org/zh-cn/) 解决方案。

- [ENFI-CLI](#ENFI-CLI)
  - [安装](#安装)
  - [更新](#更新)
  - [使用](#使用)
    - [命令查询](#命令查询)
    - [拉取模板](#拉取模板)
  - [模板](#模板)
    - [客户端](#客户端)
    - [服务端](#服务端)
  - [支持](#支持)

<!-- 关于本文档中使用的术语，请见[术语表](./docs/zh-CN/%E6%9C%AF%E8%AF%AD%E8%A1%A8.md) -->

## 安装

```bash
npm install -g enfi-cli
```


## 更新

```bash
npm update -g enfi-cli
```

## 使用

### 命令查询

```bash
enfi
```

将会输出以下的帮助信息：
```text
👽 enfi-cli v1.0.0

Commands:
  create <projectName>  创建项目
```

### 拉取模板

```bash
enfi create example
```

将会输出以下的帮助信息：
```bash
CLI v1.0.0
✔ 初始化项目
? 请输入项目名称： (example) 
# 可输入项目名称，回车确认
```

将会输出以下的帮助信息：
```bash
? 请选择模板类型： (Use arrow keys)
❯ 客户端 - Vue2.x + Antd + Js 
  服务端 - Microservice app boilerplate 
# 方向键切换选项，回车确认
```

将会输出以下的帮助信息：
```bash
✔ 🔗 Successfully created project example


      cd example
      npm install
      npm run serve
# 拉取模板成功
```

## 模板

内置模板 `客户端` 和 `服务端`。

### 客户端

```bash
# 使用说明
```

### 服务端

```bash
# 使用说明
```

## 支持
---
当前版本的 CLI 在 LTS 版本的 Node 上完全支持。Node 版本应不低于v12。
建议使用最新的长期支持版本。