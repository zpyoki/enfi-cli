# vue-cli

## Project setup
```
yarn
```

### Compiles and hot-reloads for development
```bash
yarn serve
# OR
yarn dev
# OR
yarn prod
```

### Compiles and minifies for production
```
yarn build
```

### vue项目结构

<!-- 1、build：构建脚本目录
    1）build.js ==> 生产环境构建脚本；
    2）check-versions.js ==> 检查npm，node.js版本；
    3）utils.js ==> 构建相关工具方法；
    4）vue-loader.conf.js ==> 配置了css加载器以及编译css之后自动添加 前缀；
    5）webpack.base.conf.js ==> webpack基本配置；
    6）webpack.dev.conf.js ==> webpack开发环境配置；
    7）webpack.prod.conf.js ==> webpack生产环境配置； -->

3、node_modules：npm 加载的项目依赖模块
4、src：这里是我们要开发的目录，基本上要做的事情都在这个目录里。里面包含了几个目录及文件：
    1）assets：资源目录，放置一些图片或者公共js、公共css。但是因为它们属于代码目录下，所以可以用 webpack 来操作和处理。意思就是你可以使用一些预处理比如 Sass/SCSS 或者 Stylus。
    2）components：用来存放自定义组件的目录，目前里面会有一个示例组件。
    3）router：前端路由目录，我们需要配置的路由路径写在index.js里面；
    4）App.vue：根组件；这是 Vue 应用的根节点组件，往下看可以了解更多关注 Vue 组件的信息。
    5）main.js：应用的入口文件。主要是引入vue框架，根组件及路由设置，并且定义vue实例，即初始化 Vue 应用并且制定将应用挂载到index.html 文件中的哪个 HTML 元素上。通常还会做一些注册全局组件或者添额外的 Vue 库的操作。
    6)helper：项目配置,接口配置
    7)views: 用来存放界面展示组件
5、index.html：首页入口文件，可以添加一些 meta 信息等。 这是应用的模板文件，Vue 应用会通过这个 HTML 页面来运行，也可以通过 lodash 这种模板语法在这个文件里插值。 注意：这个不是负责管理页面最终展示的模板，而是管理 Vue 应用之外的静态 HTML 文件，一般只有在用到一些高级功能的时候才会修改这个文件。
6、package.json：npm包配置文件，定义了项目的npm脚本，依赖包等信息
7、README.md：项目的说明文档，markdown 格式
<!-- 9、.xxxx文件：这些是一些配置文件，包括语法配置，git配置等
    1）.babelrc:babel编译参数
    2）.editorconfig:编辑器相关的配置，代码格式
    3）.eslintignore : 配置需要或略的路径，一般build、config、dist、test等目录都会配置忽略
    4）.eslintrc.js : 配置代码格式风格检查规则
    5）.gitignore:git上传需要忽略的文件配置
    6）.postcssrc.js ：css转换工具 --> 

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 系统类型配置
> **配置文件：** `helper/config.js`
> **配置项：** 
> **1、系统类型：systemType**
> 1）`systemType: 'default'` 默认普通系统-登录不分离
> 2）`systemType: 'sso'` 门户认证平台-为子业务系统提供登录
> 3）`systemType: 'business'`  普通业务子系统-系统从门户认证平台统一完成登录行为
> **2、统一认证平台地址：ssoUrl**（eg:http://localhost:8080）
> **3、默认跳转的子业务系统地址：redirectUrl**（eg:http://localhost:8081,恩菲后台管理系统添加子系统配置项）
> **4、默认子系统Id：clientId** （恩菲后台管理系统添加子系统配置项）
> **5、默认子系统密匙：clientSecret**（恩菲后台管理系统添加子系统配置项）

### 权限管理
==用户角色为admin时，拥有所有路由和按钮权限==
#### 1、 路由权限

> 通过配置路由参数**meta:{ permission:'xxxx' }** 控制；登录后获取用户信息接口，拿到接口返回数据中的**permissions:['aaa','bbb',...]** 参数，遍历permissions匹配到相应路由的permission路由访问权限放开，否则不能访问（具体实现逻辑示例在`store/modules/permission.js`文件中）
#### 2、按钮权限
> 方式一：在相应按钮组件中使用 `v-auth="xxx"`指令，当用户信息的permissions参数中包含v-auth配置的参数，按钮权限放开。否则不显示该按钮
> 方式二：在相应按钮组件中使用`v-if="hasPermission('xxx')"`，匹配逻辑与方式一相同
>ps： 具体实现示例在`views/permission/AuthPageC.vue`

### 其他配置

> 1、用户登录、退出、获取用户信息、角色、token校验等功能实现在`store/modules/user.js`文件中
> 2、路由拦截逻辑实现在`src/permission.js`文件中
> 3、...后续完善....